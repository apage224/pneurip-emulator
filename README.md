# pneuRIP Emulator


PneuripEmulator.playground is a Swift Playground for MacOS that acts as a BLE peripheral- emulating the main services and characteristics of a pneuRIP device.

### Usage

1. First, open the Swift Playground in Xcode. `open PneuripEmulator.playground`

1. Next, run entire playground ( ⇧⌘↩︎ ). If successful, a single **NSViewController** will be presented in the **Live View**. <br/> _NOTE: This may take a few minutes on first start while sources are compiled._

1. To begin emulating, press `Turn on` button in the view.

1. To stop emulating, press `Turn off` button in the view.


![](https://media.giphy.com/media/ftY1pp3jZoSNx06d93/giphy.gif)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Maintainers

- [Adam Page]()
- [CMD](https://www.cmdfab.com)
