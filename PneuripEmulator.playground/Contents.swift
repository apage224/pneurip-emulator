//: Playground - noun: a place where people can play

import Cocoa
import CoreBluetooth
import PlaygroundSupport
import AppKit


PlaygroundPage.current.needsIndefiniteExecution = true

print("Please press `Turn On` to start running 👍")
let viewController = PeripheralViewController()
PlaygroundPage.current.liveView = viewController

