import CoreBluetooth

public class RespiService: ServiceController {
    public let service: CBMutableService
    let respiCharacteristic = RespiCharacteristic()
    let respiOnTimeCharacteristic = RespiOnTimeCharacteristic()
    let patientFirstNameCharacteristic = PatientFirstNameCharacteristic()
    let patientLastNameCharacteristic = PatientLastNameCharacteristic()
    let patientGenderCharacteristic = PatientGenderCharacteristic()
    let patientDOBCharacteristic = PatientDOBCharacteristic()
    let patientCodeCharacteristic = PatientCodeCharacteristic()
    let patientDetailsCharacteristic = PatientDetailsCharacteristic()


    public init(service: CBMutableService = ServiceFactory.makeRespiService()) {
        self.service = service
        service.characteristics = [
            respiCharacteristic.characteristic,
            respiOnTimeCharacteristic.characteristic,
            patientFirstNameCharacteristic.characteristic,
            patientLastNameCharacteristic.characteristic,
            patientGenderCharacteristic.characteristic,
            patientDOBCharacteristic.characteristic,
            patientCodeCharacteristic.characteristic,
            patientDetailsCharacteristic.characteristic
        ]
    }

    public func handleReadRequest(_ request: CBATTRequest, peripheral: CBPeripheralManager) {
        switch (request.characteristic.uuid) {
            case respiCharacteristic.characteristic.uuid:
                self.respiCharacteristic.handleReadRequest(request, peripheral: peripheral)
            case respiOnTimeCharacteristic.characteristic.uuid:
                self.respiOnTimeCharacteristic.handleReadRequest(request, peripheral: peripheral)
            case patientFirstNameCharacteristic.characteristic.uuid:
                patientFirstNameCharacteristic.handleReadRequest(request, peripheral: peripheral)
            case patientLastNameCharacteristic.characteristic.uuid:
                patientLastNameCharacteristic.handleReadRequest(request, peripheral: peripheral)
            case patientGenderCharacteristic.characteristic.uuid:
                patientGenderCharacteristic.handleReadRequest(request, peripheral: peripheral)
            case patientDOBCharacteristic.characteristic.uuid:
                patientDOBCharacteristic.handleReadRequest(request, peripheral: peripheral)
            case  patientCodeCharacteristic.characteristic.uuid:
                patientCodeCharacteristic.handleReadRequest(request, peripheral: peripheral)
            case patientDetailsCharacteristic.characteristic.uuid:
                patientDetailsCharacteristic.handleReadRequest(request, peripheral: peripheral)
            default:
                fatalError("Invalid request")
        }
            
        // guard request.characteristic.uuid == respiCharacteristic.characteristic.uuid else {  }
        // respiCharacteristic.handleReadRequest(request, peripheral: peripheral)
    }

    public func handleSubscribeToCharacteristic(characteristic: CBCharacteristic, on peripheral: CBPeripheralManager) {
        switch (characteristic.uuid) {
            case respiCharacteristic.characteristic.uuid:
                self.respiCharacteristic.handleSubscribeToCharacteristic(on: peripheral)
            case respiOnTimeCharacteristic.characteristic.uuid:
                self.respiOnTimeCharacteristic.handleSubscribeToCharacteristic(on: peripheral)
            case patientFirstNameCharacteristic.characteristic.uuid:
                patientFirstNameCharacteristic.handleSubscribeToCharacteristic(on: peripheral)
            case patientLastNameCharacteristic.characteristic.uuid:
                patientLastNameCharacteristic.handleSubscribeToCharacteristic(on: peripheral)
            case patientGenderCharacteristic.characteristic.uuid:
                patientGenderCharacteristic.handleSubscribeToCharacteristic(on: peripheral)
            case patientDOBCharacteristic.characteristic.uuid:
                patientDOBCharacteristic.handleSubscribeToCharacteristic(on: peripheral)
            case  patientCodeCharacteristic.characteristic.uuid:
                patientCodeCharacteristic.handleSubscribeToCharacteristic(on: peripheral)
            case patientDetailsCharacteristic.characteristic.uuid:
                patientDetailsCharacteristic.handleSubscribeToCharacteristic(on: peripheral)
            default:
                fatalError("Invalid request")
        }
        // guard characteristic.uuid == respiCharacteristic.characteristic.uuid else { fatalError("Invalid request") }
        // respiCharacteristic.handleSubscribeToCharacteristic(on: peripheral)
        // print("\(#function) on \(peripheral)")
    }

    public func handleWriteRequest(_ request: CBATTRequest, peripheral: CBPeripheralManager) -> Bool {
        print("RespiService:handleWriteRequest")
        switch (request.characteristic.uuid) {
            case respiCharacteristic.characteristic.uuid:
                return self.respiCharacteristic.handleWriteRequest(request, peripheral: peripheral)
            case respiOnTimeCharacteristic.characteristic.uuid:
                return self.respiOnTimeCharacteristic.handleWriteRequest(request, peripheral: peripheral)
            case patientFirstNameCharacteristic.characteristic.uuid:
                return patientFirstNameCharacteristic.handleWriteRequest(request, peripheral: peripheral)
            case patientLastNameCharacteristic.characteristic.uuid:
                return patientLastNameCharacteristic.handleWriteRequest(request, peripheral: peripheral)
            case patientGenderCharacteristic.characteristic.uuid:
                return patientGenderCharacteristic.handleWriteRequest(request, peripheral: peripheral)
            case patientDOBCharacteristic.characteristic.uuid:
                return patientDOBCharacteristic.handleWriteRequest(request, peripheral: peripheral)
            case  patientCodeCharacteristic.characteristic.uuid:
                return patientCodeCharacteristic.handleWriteRequest(request, peripheral: peripheral)
            case patientDetailsCharacteristic.characteristic.uuid:
                return patientDetailsCharacteristic.handleWriteRequest(request, peripheral: peripheral)
            default:
                fatalError("Invalid request")
        }
    }
}
