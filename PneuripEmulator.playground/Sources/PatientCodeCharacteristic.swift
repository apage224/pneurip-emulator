import Foundation
import CoreBluetooth

class PatientCodeCharacteristic: CharacteristicController {
    let characteristic: CBMutableCharacteristic
    let respiManager: RespiManager
    private var peripheral: CBPeripheralManager?

    init(characteristic: CBMutableCharacteristic = CharacteristicFactory.makePatientCodeCharacteristic(),
        respiManager: RespiManager = .shared) {
        self.respiManager = respiManager
        self.characteristic = characteristic
    }

    func handleReadRequest(_ request: CBATTRequest, peripheral: CBPeripheralManager) {
        guard let device = respiManager.devices.values.first else { return }
        request.value = device.patient.code.data(using: .utf8)
        peripheral.respond(to: request, withResult: .success)
    }

    func handleSubscribeToCharacteristic(on peripheral: CBPeripheralManager) { self.peripheral = peripheral }

    func handleWriteRequest(_ request: CBATTRequest, peripheral: CBPeripheralManager) -> Bool {
        guard var device = respiManager.devices.values.first else { return false }
        if let data = request.value {
            device.patient.code = String(data: data, encoding: .utf8) ?? ""
            respiManager.addDevice(device)
        }
        return true
    }
}

