import CoreBluetooth

class RespiCharacteristic: RespiManagerDelegate, CharacteristicController {
    
    let characteristic: CBMutableCharacteristic
    let respiManager: RespiManager
    private var peripheral: CBPeripheralManager?
    
    init(characteristic: CBMutableCharacteristic = CharacteristicFactory.makeRespiCharacteristic(),
         respiManager: RespiManager = .shared) {
        self.respiManager = respiManager
        self.characteristic = characteristic
        respiManager.delegate = self
    }
    
    func handleReadRequest(_ request: CBATTRequest, peripheral: CBPeripheralManager) {
        guard let device = respiManager.devices.values.first else { return }
        request.value = device.rcab
        peripheral.respond(to: request, withResult: .success)
    }
    
    func handleSubscribeToCharacteristic(on peripheral: CBPeripheralManager) {
        self.peripheral = peripheral
    }
    
    func handleWriteRequest(_ request: CBATTRequest, peripheral: CBPeripheralManager) -> Bool { return true }
    
    func respiManager(_ respiManager: RespiManager, didUpdateRespi respiModel: RespiModel) {
        // print("Updating value: \(respiModel), on: \(String(describing: peripheral))")
        peripheral?.updateValue(respiModel.rcab, for: characteristic, onSubscribedCentrals: nil)
    }
}

