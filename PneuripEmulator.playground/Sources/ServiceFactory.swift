import CoreBluetooth

public class ServiceFactory {
    public static func makeRespiService() -> CBMutableService {
        return CBMutableService(type: CBUUID(string: "EECB7DB8-8B2D-402C-B995-825538B49328"), primary: true)
    }    
}
