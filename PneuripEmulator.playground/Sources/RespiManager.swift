import Foundation

extension UInt16 {
    var data: Data {
        var int = self
        return Data(bytes: &int, count: MemoryLayout<UInt16>.size)
    }
}

extension UInt32 {
    var data: Data {
        var int = self
        return Data(bytes: &int, count: MemoryLayout<UInt32>.size)
    }
}

extension Int32 {
    var data: Data {
        var int = self
        return Data(bytes: &int, count: MemoryLayout<Int32>.size)
    }
}

public protocol RespiManagerDelegate: class {
    func respiManager(_ respiManager: RespiManager, didUpdateRespi respiModel: RespiModel)
}

public class RespiManager {
    
    public static let shared = RespiManager()
    
    public private(set) var devices: [String: RespiModel] = [:] {
        didSet {
            devices.values.forEach({ delegate?.respiManager(self, didUpdateRespi: $0) })
        }
    }
    public weak var delegate: RespiManagerDelegate?
    public var timer: Timer?
    public var t: Float = 0.0
    public func addDevice(_ device: RespiModel) {
        devices[device.name] = device
    }
    
    public func changeBandData(deviceName: String, data: Data) {
        devices[deviceName]?.rcab = data
        devices[deviceName]?.ontime = UInt32(t)
    }
    
    public func turnOn() {
        self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateBandData), userInfo: nil, repeats: true)
    }
    
    public func turnOff() {
        self.timer?.invalidate()
    }
    
    @objc public func updateBandData() {
        let FS: Float = 10.0
        let rcA: Float = 200
        let abA: Float = 100
        let rcF: Float = 25/60
        let abF: Float = 25/60
        let rcPHI: Float = 0*Float.pi
        let abPHI: Float = 0*Float.pi
        var rcabData = Data(repeating: 0, count: 20)
        for n in 0...4 {
            let rc_val = rcPHI + 2*Float.pi*rcF*self.t
            let ab_val = abPHI + 2*Float.pi*abF*self.t
            let rc = UInt16(rcA*cos(rc_val) +  5000.0)
            let ab = UInt16(abA*cos(ab_val) + 10000.0)
            let rcData = rc.data
            let abData = ab.data
            rcabData[2*n+0] = rcData[0]
            rcabData[2*n+1] = rcData[1]
            rcabData[10+2*n+0] = abData[0]
            rcabData[10+2*n+1] = abData[1]
            self.t += 1.0/FS
            if self.t > 10000000 { self.t = 0 }
        }        
        devices.values.forEach({ self.changeBandData(deviceName: $0.name, data:rcabData)})
        //print("HELLO")
    }
}
