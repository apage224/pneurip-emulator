import CoreBluetooth

protocol CharacteristicController {
    var characteristic: CBMutableCharacteristic { get }
    func handleReadRequest(_ request: CBATTRequest, peripheral: CBPeripheralManager)
    func handleWriteRequest(_ request: CBATTRequest, peripheral: CBPeripheralManager) -> Bool
    func handleSubscribeToCharacteristic(on peripheral: CBPeripheralManager)
}

public class CharacteristicFactory {
    // Respi Service Characteristics
    public static func makeRespiCharacteristic() -> CBMutableCharacteristic {
        return CBMutableCharacteristic(type: CBUUID(string: "3F2C1908-35BA-49E6-BDF1-D8C73625CE3C"), properties: [.read, .notify], value: nil, permissions: [.readable])
    }
    public static func makeRespiOnTimeCharacteristic() -> CBMutableCharacteristic {
        return CBMutableCharacteristic(type: CBUUID(string: "9E58D6E3-E159-4880-A4AD-06C579B7A0ED"), properties: [.read, .notify], value: nil, permissions: [.readable])
    }
    
    // Patient Service Characteristics
    public static func makePatientFirstNameCharacteristic() -> CBMutableCharacteristic {
        return CBMutableCharacteristic(type: CBUUID(string: "CE6CBC18-C892-4097-932F-1780F69497DE"), properties: [.read, .write, .notify], value: nil, permissions: [.readable, .writeable])
    }
    public static func makePatientLastNameCharacteristic() -> CBMutableCharacteristic {
        return CBMutableCharacteristic(type: CBUUID(string: "1DEE9CA4-24C2-4867-BA3E-8AB4F6E0974C"), properties: [.read, .write, .notify], value: nil, permissions: [.readable, .writeable])
    }
    public static func makePatientGenderCharacteristic() -> CBMutableCharacteristic {
        return CBMutableCharacteristic(type: CBUUID(string: "A4AA02E4-890D-4F08-A42D-BB6FE13E065D"), properties: [.read, .write, .notify], value: nil, permissions: [.readable, .writeable])
    }
    public static func makePatientCodeCharacteristic() -> CBMutableCharacteristic {
        return CBMutableCharacteristic(type: CBUUID(string: "8BF95EAF-E94D-48A0-9B55-8B454533AD4D"), properties: [.read, .write, .notify], value: nil, permissions: [.readable, .writeable])
    }
    public static func makePatientDOBCharacteristic() -> CBMutableCharacteristic {
        return CBMutableCharacteristic(type: CBUUID(string: "FB2F680E-000C-447F-916D-4F9939217ABB"), properties: [.read, .write, .notify], value: nil, permissions: [.readable, .writeable])
    }
    public static func makePatientDetailsCharacteristic() -> CBMutableCharacteristic {
        return CBMutableCharacteristic(type: CBUUID(string: "43AA8B13-3548-4D88-8127-EBF1691CA118"), properties: [.read, .write, .notify], value: nil, permissions: [.readable, .writeable])
    }
}
