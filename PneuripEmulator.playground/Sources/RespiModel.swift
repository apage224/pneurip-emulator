import Foundation

public struct PatientModel {
    public var firstName: String
    public var lastName: String
    public var gender: String
    public var dob: String
    public var code: String
    public var details: String
    public init() {
        firstName = "John"
        lastName = "Doe"
        gender = "M"
        dob = "01/20/1989"
        code = "011235"
        details = "..."
    }
}

public struct RespiModel {
    public var rcab: Data
    public var ontime: UInt32
    public let name: String
    public var patient: PatientModel
    public init(name: String) {
        self.name = name
        self.rcab = Data(repeating: 0, count: 10)
        self.ontime = 0        
        self.patient = PatientModel()
    }
}
