import Foundation
import CoreBluetooth

class RespiOnTimeCharacteristic: CharacteristicController {
    let characteristic: CBMutableCharacteristic
    let respiManager: RespiManager
    private var peripheral: CBPeripheralManager?

    init(characteristic: CBMutableCharacteristic = CharacteristicFactory.makeRespiOnTimeCharacteristic(),
        respiManager: RespiManager = .shared) {
        self.respiManager = respiManager
        self.characteristic = characteristic
    }

    func handleReadRequest(_ request: CBATTRequest, peripheral: CBPeripheralManager) {
        guard let device = respiManager.devices.values.first else { return }
        request.value = device.ontime.data
        peripheral.respond(to: request, withResult: .success)
    }

    func handleSubscribeToCharacteristic(on peripheral: CBPeripheralManager) { self.peripheral = peripheral }

    func handleWriteRequest(_ request: CBATTRequest, peripheral: CBPeripheralManager) -> Bool { return true }
}
